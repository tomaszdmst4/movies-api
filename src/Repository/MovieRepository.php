<?php

namespace App\Repository;

use App\Entity\Movie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
/**
 * @method Movie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movie[]    findAll(array )
 * @method Movie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieRepository extends ServiceEntityRepository
{
    private $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Movie::class);
        $this->entityManager = $entityManager;
    }

    public function saveMovie($title, $originalTitle, $titleYear, $description, $genre, $director): Movie
    {
        $newMovie = new Movie();

        $newMovie
            ->setTitle($title)
            ->setOriginalTitle($originalTitle)
            ->setTitleYear($titleYear)
            ->setDescription($description)
            ->setGenre($genre)
            ->setDirector($director);

        $this->entityManager->persist($newMovie);
        $this->entityManager->flush();

        return $newMovie;
    }

    public function updateMovie(Movie $movie): Movie
    {
        $this->entityManager->persist($movie);
        $this->entityManager->flush();

        return $movie;

    }

    public function removeMovie(Movie $movie)
    {
        $this->entityManager->remove($movie);
        $this->entityManager->flush();
    }

    public function getAllMoviesAssoc(): array
    {
        return $this->entityManager->createQueryBuilder()
            ->select('m')
            ->from('App:Movie', 'm', 'm.id')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }

}
