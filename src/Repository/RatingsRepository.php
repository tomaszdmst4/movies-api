<?php

namespace App\Repository;

use App\Entity\Ratings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ratings|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ratings|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ratings[]    findAll()
 * @method Ratings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RatingsRepository extends ServiceEntityRepository
{
    private $entityManager;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Ratings::class);
        $this->entityManager = $entityManager;
    }

    public function rateMovie($userId, $movieId, $ratingValue)
    {

        $rating = new Ratings();

        $rating
            ->setMovieId($movieId)
            ->setUserId($userId)
            ->setRatingValue($ratingValue);
        $this->entityManager->persist($rating);
        $this->entityManager->flush();

        return $rating;

    }

    public function updateRating(Ratings $rating): Ratings
    {
        $this->entityManager->persist($rating);
        $this->entityManager->flush();

        return $rating;

    }

    public function checkRating($movieId, $userId): array
        {
            try {
                return $this->createQueryBuilder('r')
                    ->select("count(r.id) as count")
                    ->where('r.userId = :userId')
                    ->andWhere('r.movieId = :movieId')
                    ->setParameter('userId', $userId)
                    ->setParameter('movieId', $movieId)
                    ->getQuery()
                    ->getOneOrNullResult();
            } catch (NonUniqueResultException $e) {
                return $e;
            }
        }

    public function checkAverageRating($movieId): array
    {
        try {
            return $this->createQueryBuilder('r')
                ->select("avg(r.ratingValue) as avg")
                ->where('r.movieId = :movieId')
                ->setParameter('movieId', $movieId)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return $e;
        }
    }
}
