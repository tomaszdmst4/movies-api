<?php


namespace App\Controller;


use App\Repository\MovieRepository;
use App\Repository\RatingsRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \Doctrine\DBAL\DBALException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ApiController
 * @package App\Controller
 * @Route("/api")
 */
class ApiController
{
    private $movieRepository;
    private $ratingsRepository;
    private $authorization;

    public function __construct(MovieRepository $movieRepository,
                                RatingsRepository $ratingsRepository,
                                AuthorizationCheckerInterface $authorization)
    {
        $this->movieRepository = $movieRepository;
        $this->ratingsRepository = $ratingsRepository;
        $this->authorization = $authorization;
    }



    /**
     * @Route("/movie/add", name="add_movie", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function add(Request $request): JsonResponse
    {

        $data = json_decode($request->getContent(), true);

        $title         = $data['title'];
        $originalTitle = $data['originalTitle'];
        $titleYear     = $data['titleYear'];
        $description   = $data['description'];
        $genre         = $data['genre'];
        $director      = $data['director'];

        if (empty($title) || empty($originalTitle) || empty($titleYear) || empty($description) || empty($genre) ||
            empty($director))
        {
            return new JsonResponse(['status' => 'Invalid parameters'], Response::HTTP_BAD_REQUEST);
        }
        else
        {
            $movie = $this->movieRepository->saveMovie($title, $originalTitle, $titleYear, $description, $genre, $director);

            return new JsonResponse(['status' => 'Movie added', 'id' => $movie->getId()], Response::HTTP_CREATED);
        }

    }



    /**
     * @Route("/movie/update/{id}", name="update_movie", methods={"PATCH"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update($id, Request $request): JsonResponse
    {
        $movie = $this->movieRepository->findOneBy(['id' => $id]);

        $data = json_decode($request->getContent(), true);

        empty($data['title']) ? true : $movie->setTitle($data['title']);
        empty($data['originalTitle']) ? true : $movie->setOriginalTitle($data['originalTitle']);
        empty($data['titleYear']) ? true : $movie->setTitleYear($data['titleYear']);
        empty($data['description']) ? true : $movie->setDescription($data['description']);
        empty($data['genre']) ? true : $movie->setGenre($data['genre']);
        empty($data['director']) ? true : $movie->setDirector($data['director']);

        try {
            $updatedMovie = $this->movieRepository->updateMovie($movie);
            return new JsonResponse($updatedMovie->toArray(), Response::HTTP_OK);
        }
        catch (DBALException $exception)
        {
            return new JsonResponse([
                'status'=>'Internal Server Error',
                'error' => $exception->getMessage()
            ],
                Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @Route("/movie/delete/{id}", name="delete_movie", methods={"DELETE"})
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        $movie = $this->movieRepository->findOneBy(['id' => $id]);

        if($movie) {
            $this->movieRepository->removeMovie($movie);
            return new JsonResponse(['status' => 'Movie deleted', Response::HTTP_OK]);
        }
        else
        {
            return new JsonResponse(['status' => 'Movie does not exist', Response::HTTP_OK]);
        }
    }

    /**
     * @Route("/movie/id/{id}", name="get_movie", methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function get($id): JsonResponse
    {
        $movie = $this->movieRepository->findOneBy(['id' => $id]);

        $data = [
            'id' => $movie->getId(),
            'title' => $movie->getTitle(),
            'originalTitle' => $movie->getOriginalTitle(),
            'titleYear' => $movie->getTitleYear(),
            'description' => $movie->getDescription(),
            'genre' => $movie->getGenre(),
            'director' => $movie->getDirector(),
            'averageRating' => $this->ratingsRepository->checkAverageRating($movie->getId())["avg"]
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/movies", name="movies", methods={"GET"})
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $movies = $this->movieRepository->getAllMoviesAssoc();
        return new JsonResponse($movies, Response::HTTP_OK);
    }

    /**
     * @Route("/movie/rate", name="rate_movie", methods={"POST"})
     * @param Request $request
     * @param UserInterface|null $user
     * @return JsonResponse
     */
    public function rate(Request $request, UserInterface $user = null): JsonResponse
    {

        $MIN_RATING = 1;
        $MAX_RATING = 10;


        $data = json_decode($request->getContent(), true);

        $userId          = $user->getId();
        $movieId         = $data['movieId'];
        $ratingValue     = $data['ratingValue'];


        $movie = $this->movieRepository->findOneBy(['id' => $movieId]);


        if (empty($userId) || empty($movieId) || empty($ratingValue) || empty($movie))
        {
            return new JsonResponse(['status' => 'Invalid parameters'], Response::HTTP_BAD_REQUEST);
        }
        else
        {
            if($ratingValue >= $MIN_RATING && $ratingValue <= $MAX_RATING)
            {

                $count = $this->ratingsRepository->checkRating($movieId, $user->getId())["count"];

                if($count == 0){
                    $this->ratingsRepository->rateMovie($userId, $movieId, $ratingValue);

                    return new JsonResponse(['status' => 'Movie rated'], Response::HTTP_CREATED);
                }
                else{

                    $rating = $this->ratingsRepository->findOneBy(['userId' => $userId, 'movieId' => $movieId]);

                    empty($data['ratingValue']) ? true : $rating->setRatingValue($ratingValue);
                    $this->ratingsRepository->updateRating($rating);

                    return new JsonResponse(['status' => 'Rating updated'], Response::HTTP_OK);
                }
            }
            else
            {
                return new JsonResponse(['status' => 'Incorrect value'], Response::HTTP_BAD_REQUEST);
            }
        }
    }

}