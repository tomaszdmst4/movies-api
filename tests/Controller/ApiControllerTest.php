<?php

namespace App\Tests\Controller;

use \GuzzleHttp\Client;
use \GuzzleHttp;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class ApiControllerTest extends WebTestCase
{
    private $movieId;
    private $auth_token;
    public $username = 'testuser';
    public $password = 'testpassword';

    public function testRegistration()
    {
        $client = new Client(['base_uri' => 'http://localhost:8000']);

        try {
            $response = $client->request('POST', '/api/auth/register', [
                'json' =>   [
                    "username" => $this->username,
                    "password" => $this->password,
                ]]);
            $this->assertEquals(201, $response->getStatusCode());
        }
        catch (GuzzleHttp\Exception\BadResponseException  $e) {
            $response = $e->getResponse();
            $this->assertEquals(400, $response->getStatusCode());

        }

    }

    public function testAuth()
    {
        $client = new Client(['base_uri' => 'http://localhost:8000']);

        $auth_token = $client->request('POST', '/api/login_check', [
            'json' =>   [
                "username" => $this->username,
                "password" => $this->password,
            ]]);
        $auth_data = json_decode($auth_token->getBody(true), true);
        $this->auth_token = $auth_data['token'];
        $this->assertEquals(200, $auth_token->getStatusCode());
    }

    public function testAddMovie()
    {
        $client = new Client(['base_uri' => 'http://localhost:8000']);

        $rand = rand(1, 1000000);
        $this->testAuth();

        $response = $client->request('POST', '/api/movie/add', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization'     => 'Bearer '.$this->auth_token
            ],
            'json' =>   [
                "title" => $rand,
                "originalTitle" => $rand,
                "titleYear" => 2020,
                "description" => $rand,
                "genre" => $rand,
                "director" => $rand
            ]]);

        $this->assertEquals(201, $response->getStatusCode());
        $data = json_decode($response->getBody(true), true);

        $this->movieId = $data['id'];

        $this->assertIsArray($data);
    }



    public function testGetAll()
    {
        $client = new Client(['base_uri' => 'http://localhost:8000']);

        $response = $client->get('/api/movies');

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(true), true);
        $this->assertIsArray($data);
    }

    public function testGetNoAuth()
    {
        $client = new Client(['base_uri' => 'http://localhost:8000']);

        $this->testAddMovie();

        try {
            $client->get('/api/movie/id/' . $this->movieId);
        }
        catch (GuzzleHttp\Exception\BadResponseException  $e) {
            $response = $e->getResponse();
            $this->assertEquals(401, $response->getStatusCode());
        }
    }

   public function testGetWithAuth()
    {
        $client = new Client(['base_uri' => 'http://localhost:8000']);

        $this->testAuth();
        $this->testAddMovie();

        $request = $client->request('GET','/api/movie/id/'. $this->movieId,
            ['headers' => [
                'Content-Type' => 'application/json',
                'Authorization'     => 'Bearer '.$this->auth_token
            ]
        ]);
        $this->assertEquals(200, $request->getStatusCode());
        $data = json_decode($request->getBody(true), true);
        $this->assertArrayHasKey('title', $data);
    }

    public function testDeleteMovie()
    {
        $client = new Client(['base_uri' => 'http://localhost:8000']);

        $this->testAuth();
        $this->testAddMovie();

        $request = $client->request('DELETE','/api/movie/delete/'. $this->movieId,
            ['headers' => [
                'Content-Type' => 'application/json',
                'Authorization'     => 'Bearer '.$this->auth_token
            ]
            ]);
        $this->assertEquals(200, $request->getStatusCode());
    }

    public function testRateMovie()
    {
        $client = new Client(['base_uri' => 'http://localhost:8000']);

        $this->testAuth();
        $this->testAddMovie();

        $request = $client->request('POST','/api/movie/rate', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization'     => 'Bearer '.$this->auth_token
            ],
                'json' =>   [
                    "movieId" => $this->movieId,
                    "ratingValue" => rand(1, 10)
                ]
            ]);
        $this->assertEquals(201, $request->getStatusCode());
    }

}
