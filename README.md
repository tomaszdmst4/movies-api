
Konfiguracja bazy MySQL
```
DATABASE_URL=mysql://root:123456@127.0.0.1:3306/movies?serverVersion=5.7
```


```
php composer update
bin/console doctrine:migrations:migrate
```

Routing aplikacji
```
-------------------------- -------- -------- ------ -----------------------------------
  Name                       Method   Scheme   Host   Path                              
 -------------------------- -------- -------- ------ -----------------------------------
  add_movie           POST     ANY      ANY    /api/movie/add
  update_movie        PATCH    ANY      ANY    /api/movie/update/{id}
  delete_movie        DELETE   ANY      ANY    /api/movie/delete/{id}
  get_movie           GET      ANY      ANY    /api/movie/id/{id}
  movies              GET      ANY      ANY    /api/movies
  rate_movie          POST     ANY      ANY    /api/movie/rate
  user_registration   POST     ANY      ANY    /api/auth/register
  api_login_check     ANY      ANY      ANY    /api/login_check
 -------------------------- -------- -------- ------ -----------------------------------

```

Testy
```
php bin/phpunit tests/Controller/ApiControllerTest.php
```

Rejestrowanie konta użytkownika:
```
curl -X POST -H "Content-Type: application/json" http://localhost:8000/api/auth/register -d '{"username":"login","password":"haslo"}'
```

Postman:
```
http://localhost:8000/api/auth/register

{
	"username" : "login",
	"password" : "haslo"
}
```

Generowanie tokena JWT
```
curl -X POST -H "Content-Type: application/json" http://localhost:8000/api/login_check -d '{"username":"login","password":"haslo"}'
```

Postman:
```
http://localhost:8000/api/login_check

{
	"username" : "login",
	"password" : "haslo"
}
```

W przypadku ścieżek wymagających autoryzacji, do nagłówka zapytania należy dodać token JWT
```
KEY: 
    Authorization
VALUE:
    Bearer {JWT TOKEN}
```

Np. :
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYX(...)